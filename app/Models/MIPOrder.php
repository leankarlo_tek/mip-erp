<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class MIPOrder extends Model
{

    protected $table = 'mip_orders';

    public function orderDetails()
    {
        return $this->hasMany('App\Models\MIPOrderDetail', 'order_id');
    }

    public function customer()
    {
        return $this->hasOne('App\Models\MIPCustomer', 'id', 'customer_id');
    }

    public function user()
    {
        return $this->hasOne('App\Models\User', 'id','created_by');
    }

    public function payment()
    {
        return $this->hasOne('App\Models\MIPPayment', 'order_id');
    }

    public function paidOrders()
    {
        return $this->hasOne('App\Models\MIPPayment', 'order_id')->where('status',3);
    }

    public function status()
    {
        return $this->hasOne('App\Models\MIPOrderStatus', 'id', 'order_status');
    }

    public function order_status()
    {
        return $this->hasOne('App\Models\MIPOrderStatus', 'id', 'status');
    }

}
