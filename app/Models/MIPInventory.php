<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class MIPInventory extends Model
{

    protected $table = 'mip_inventory';
    protected $casts = [ 
    'name' => 'String', 
    'description' => 'String'
    ];

    public function stock()
    {
        return $this->hasMany('App\Models\MIPInventoryStock', 'inventory_id', 'id');
    }

}
