<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class MIPPaymentDetail extends Model
{
	protected $table = 'mip_payment_details';

	protected $casts = [ 
    'order_id' => 'String', 
    'payment_type' => 'String', 
    'amount' => 'String', 
    'fee' => 'String',
    'branch_id' => 'String'
    ];

    public function paymentDetails()
    {
        return $this->hasOne('App\Models\MIPPayment', 'id');
    }

    public function paymentMethod()
    {
        return $this->hasMany('App\Models\MIPPaymentType', 'id', 'type');
    }

}
