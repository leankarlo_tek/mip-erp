<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class MIPCustomer extends Model
{

    protected $table = 'mip_customer';
    protected $casts = [ 
    'email' => 'String', 
    'firstname' => 'String', 
    'middlename' => 'String', 
    'lastname' => 'String', 
    'zipcode' => 'String', 
    'address1' => 'String',
    'address2' => 'String',
    'address3' => 'String',
    'city' => 'String',
    'state' => 'String',
    'country' => 'String',
    'company' => 'String'
    ];

    public function order()
    {
        return $this->belongsToMany('App\Models\MIPOrder', 'customer_id', 'user_id');
    }

}
