<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class MIPProductStock extends Model
{

    protected $table = 'mip_product_stocks';

    protected $casts = [ 
    'product_id' => 'String', 
    'isAvailable' => 'String',
    'created_by' => 'String'
    ];

    public function product()
	{
		return $this->belongsTo('App\Models\MIPProduct', 'product_id');
	}

    public function employee(){
         return $this->hasOne('App\Models\User', 'id', 'created_by');
    }

}
