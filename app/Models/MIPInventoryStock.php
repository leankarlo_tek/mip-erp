<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class MIPInventoryStock extends Model
{

    protected $table = 'mip_inventory_stock';
    protected $casts = [ 
        'inventory_id' => 'String', 
        'isActive' => 'String',
        'used_by' => 'String',
        'date_used' => 'String'
    ];

    public function information()
    {
        return $this->belongsTo('App\Models\MIPInventory', 'inventory_id', 'id');
    }

}
