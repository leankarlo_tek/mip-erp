<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class MIPOrderDetail extends Model
{

    protected $table = 'mip_order_details';

    protected $casts = [ 'order_id' => 'String', 'product_id' => 'String', 'product_stock_id' => 'String', 'srp' => 'String', 'quantity' => 'String', 'item_discount' => 'String', 'selling_price' => 'String' ];

    public function order()
    {
        return $this->hasOne('App\Models\MIPOrder', 'id');
    }

    public function customer()
    {
    	return $this->hasMany('App\Models\MIPCustomer', 'customer_id');
    }

    public function productStocks()
    {
        return $this->hasMany('App\Models\MIPProductStock', 'id' ,'product_stock_id');
    }

    public function product()
    {
        return $this->hasMany('App\Models\MIPProduct', 'id', 'product_id');
    }

}
