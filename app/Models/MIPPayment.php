<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class MIPPayment extends Model
{

	protected $table = 'mip_payments';

	protected $casts = [ 
    'order_id' => 'String', 
    'isPayed' => 'String',
    'branch_id' => 'String'
    ];

    public function order()
    {
        return $this->hasOne('App\Models\MIPOrder', 'id', 'order_id');
    }

    public function paymentDetails()
    {
        return $this->hasMany('App\Models\MIPPaymentDetails', 'order_id');
    }

}
