<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;
use App\Http\Controllers\API\Universal\v1\ShipmentController;

class XendShipmentStatus extends Command {
	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'xend:update_status';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Update Shipment Status';

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$shipment = new ShipmentController();
		$shipment->updateShipmentStatus();

		// \Log::info('Shipment Updated At - ( ' . \Carbon\Carbon::now() . ' )');
	}
}