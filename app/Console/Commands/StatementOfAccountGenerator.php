<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;
use App\Http\Controllers\API\Universal\v1\ShipmentController;

class StatementOfAccountGenerator extends Command {
	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'Corporate:StatementOfAccount';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Statement of account and reminder automatic generator, for corporate clients order.';

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$shipment = new ShipmentController();
		$shipment->updateShipmentStatus();

		// \Log::info('Shipment Updated At - ( ' . \Carbon\Carbon::now() . ' )');
	}
}