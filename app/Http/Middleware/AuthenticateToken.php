<?php namespace App\Http\Middleware;

use Firebase\JWT\JWT;
use Config;
use Closure;
use Illuminate\Contracts\Auth\Guard;

class AuthenticateToken {

	/**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $auth;

	/**
	 * Create a new filter instance.
	 *
	 * @param  Guard  $auth
	 * @return void
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if ($request->input('token'))
		{
			$token = urldecode($request->input('token'));
			$payload = (array) JWT::decode($token, Config::get('app.token_secret'), array('HS256'));


			$request['token'] = $payload;

			return $next($request);
		}
		else
		{
			return response()->json(['message' => 'Please make sure your request has token'], 401);
		}
	}

}
