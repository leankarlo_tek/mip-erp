<?php

namespace App\Http\Controllers\API\v1\MIP;

use App\Models\User;
use App\Models\MIPCustomer;
use Redirect;
use View;
use Response;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Users Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the Images functions
    | 1. Upload
    | 2. Edit
    | 3. Delete
    | 4. Activate
    | 5. Deactivate
    | 6. Validation
    |
    */

    // USER DISPLAYS
    protected function showAll()
    {
        $users = MIPCustomer::all();
        $returnArray = array('result' => true);
        $returnArray = array_add($returnArray, 'data' , $users);
        return Response::json($returnArray );
    }

    protected function createUser(Request $request) {
        //INITIALIZATION
        $input = $request->all();

        $email          = $input['email'];
        // $password       = $input['password'];//Hash::make('pw1234'),

        //check username & email if exist
        // $emailCheck = MIPCustomer::where('email', $input['email'])->get()->first();
        // if($emailCheck != null){
        //     return Response::json(array('result' => false ,'message' => 'Email Already Exist' ) );
        // }

        $customer = new MIPCustomer;
        $customer->email        = $email;
        $customer->lastname     = $input['lastname'];
        $customer->firstname    = $input['firstname'];
        $customer->middlename   = $input['middlename'];
        $customer->address1     = $input['address1'];
        $customer->address2     = $input['address2'];
        $customer->state        = $input['state'];
        $customer->country      = $input['country'];
        $customer->zipcode      = $input['zipcode'];
        $customer->company      = $input['company'];
        $customer->save();

        return Response::json(array('result' => true ,'message' => $email ) );

    }


    protected function updateUser(Request $request) {
        //INITIALIZATION
        $input = $request->all();

        // $email          = $input['email'];
        // $type           = $input['type'];

        try {
            $customer = MIPCustomer::find($input['id']);

            if($request->input('email',null)      !== null) $customer->email       = $request->input('email');
            if($request->input('username',null)      !== null) $customer->username       = $request->input('username');
            // if($request->input('type',null)      !== null) $customer->type       = $request->input('type');
            if($request->input('lastname',null)      !== null) $customer->lastname       = $request->input('lastname');
            if($request->input('firstname',null)      !== null) $customer->firstname       = $request->input('firstname');
            if($request->input('middlename',null)      !== null) $customer->middlename       = $request->input('middlename');
            if($request->input('address1',null)      !== null) $customer->address1       = $request->input('address1');
            if($request->input('address2',null)      !== null) $customer->address2       = $request->input('address2');
            if($request->input('city',null)      !== null) $customer->city       = $request->input('city');
            if($request->input('state',null)      !== null) $customer->state       = $request->input('state');
            if($request->input('country',null)      !== null) $customer->country       = $request->input('country');
            if($request->input('zipcode',null)      !== null) $customer->zipcode       = $request->input('zipcode');
            if($request->input('company',null)      !== null) $customer->company       = $request->input('company');

            $customer->save();

            

            return Response::json(array('result' => true ,'message' => 'Customer Succesfully Saved!' ) );
        } catch (Exception $e) {
            return Response::json(array('result' => false ,'message' => 'Error Saving User! Contact System Admin' ) );
        }
        
    }

    protected function deleteUser(Request $request)
    {
        $input = $request->all();
        $id = $input['id'];
        $customer = MIPCustomer::find($id);

        try {
            $customer->delete();
            return Response::json( array('result' => true ,'message' => 'Customer Succesfully Deleted!' ) );

        } catch (Exception $e) {
            
        }
    }
}
