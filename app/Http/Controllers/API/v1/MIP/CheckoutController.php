<?php

namespace App\Http\Controllers\API\v1\mip;

use DB;
use App\Models\MIPProduct;
use App\Models\MIPProductStock;
use App\Models\MIPProductType;
use App\Models\MIPOrder;
use App\Models\MIPOrderDetail;
use App\Models\MIPOrderStatus;
use App\Models\MIPPayment;
use App\Models\MIPPaymentDetail;
use App\Models\MIPPaymentStatus;
use App\Models\MIPTilling;
use Redirect;
use View;
use Response;
use Auth;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CheckoutController extends Controller
{
    protected function test(Request $request)
    {
        $input = $request->all();
        return Response::json($input);
    }

    protected function show(Request $request)
    {

    }

    protected function createOrderAndPayment(Request $request) {

        //INITIALIZATION
        $input = $request->all();

        $order = new MIPOrder;
        $order->customer_id = $request->input('customer_id') ? $request->input('customer_id') : 1;
        $order->subtotal = $input['subtotal'];
        $order->discount = $request->input('discount') ? $request->input('discount') : 0;
        $order->total = $input['total'];
        $order->status = $input['type'];
        if($request->input('vat',null) !== null) $order->VAT = $request->input('vat');
        if($request->input('created_by',null) !== null) $order->created_by = $request->input('created_by');
        $order->save();

        $payment = new MIPPayment;
        $payment->order_id = $order->id;
        $payment->save();

        return Response::json(array('result' => true, 'data' => array('order_id' => $order->id, 'payment_id' => $payment->id), 'message' => 'succesfully created an order' ) );

    }

    protected function createOrderDetails(Request $request){
        
        //INITIALIZATION
        $input = $request->all();

        //validator example
        // $validator = Validator::make($request->all(), [
        //     'order_id' => 'required'
        // ]);

        // if ($validator->fails()) {
        //     return response()->json(array('result' => false, 'message' => $validator->messages()), 400);
        // }

        $order_id = $input['order_id'];

        $orders = $input['order'];

        foreach ($orders as $order) {

            for($i=0;$i < $order['quantity']; $i++)
            {
                $productStock = MIPProductStock::where('isAvailable',1)
                ->where('product_id', $order['product_id'])
                ->get()
                ->first();
                $productStock->isAvailable = 0;
                $productStock->save();
            }
                
            // - update orderdetails
            $orderDetails = new MIPOrderDetail;
            $orderDetails->order_id         = $order_id;
            $orderDetails->srp              = $order['srp'];
            $orderDetails->product_id       = $order['product_id'];
            $orderDetails->quantity         = $order['quantity'];
            $orderDetails->discount         = $request->input('discount') ? $request->input('discount') : 0;
            $orderDetails->isCrushed        = $request->input('isCrushed') ? $request->input('isCrushed') : 0;
            $orderDetails->total            = $order['total'];
            $orderDetails->save();

        }

        return Response::json(array('result' => true,  'message' => 'successfully created a order details' ) );
    }

    protected function createPaymentDetails(Request $request)
    {
        
        //INITIALIZATION
        $input = $request->all();

        //validator example
        // $validator = Validator::make($request->all(), [
        //     'order_id' => 'required'
        // ]);

        // if ($validator->fails()) {
        //     return response()->json(array('result' => false, 'message' => $validator->messages()), 400);
        // }

        $order_id = $input['order_id'];

        $payments = $input['payment'];

        $expected = 0;
        $tilt = MIPTilling::where('id',1)->first();
        $expected = $tilt->expected_till;
        foreach ($payments as $payment) {
            $paymentDetails = new MIPPaymentDetail;
            $paymentDetails->order_id       = $order_id;
            $paymentDetails->type           = $payment['type'];
            $paymentDetails->amount         = $payment['amount'];
            $paymentDetails->save();
            if( $payment['type'] == 1 || $payment['type'] == '1')
            $expected = $expected + $payment['amount'];
        }

        $tilt->expected_till = $expected;
        $tilt->save();

        return Response::json(array('result' => true,  'message' => 'successfully created a payment details' ) );
    }

    protected function updatePaymentSatus(Request $request)
    {
        
        //INITIALIZATION
        $input = $request->all();

        //validator example
        $validator = Validator::make($request->all(), [
            'order_id' => 'required',
            'status' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(array('result' => false, 'message' => $validator->messages()), 400);
        }

        $order_id   = $input['order_id'];
        $status     = $input['status'];

        $payment = MIPPayment::where('order_id', $order_id)->get()->first();
        $payment->status = $input['status'];
        $payment->save();

        return Response::json(array('result' => true,  'message' => 'successfully updated a payment isPayed status' ) );
    
    }

}
