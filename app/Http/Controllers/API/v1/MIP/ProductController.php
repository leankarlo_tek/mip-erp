<?php

namespace App\Http\Controllers\API\v1\mip;

use DB;
use App\Models\MIPProduct;
use App\Models\MIPProductStock;
use Redirect;
use View;
use Response;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;

class ProductController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Users Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the Images functions
    | 1. Upload
    | 2. Edit
    | 3. Delete
    | 4. Activate
    | 5. Deactivate
    | 6. Validation
    |
    */
    // USER DISPLAYS
    protected function show(Request $request)
    {

        $result = MIPProduct::all();

        $product = array('result' => true);
        $product = array_add($product, 'data' , $result);
        return Response::json( $product );
    }

    protected function create(Request $request)
    {

        //INITIALIZATION
        $input = $request->all();

         //validator example
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'sku' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(array('result' => false, 'message' => $validator->messages()), 400);
        }

        $model = new MIPProduct;
        $model->name    = $input['name'];
        $model->sku     = $input['sku'];
        if($request->input('description',null)      !== null) $model->description       = $request->input('description');
        if($request->input('srp',null)      !== null) $model->srp       = $request->input('srp');
        $model->save();

        $return = array('result' => true, 'message' => ' New Stock has been added');
        return Response::json( $return  );

    }
    protected function update(Request $request)
    {

        //INITIALIZATION
        $input = $request->all();

         //validator example
        $validator = Validator::make($request->all(), [
            'id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(array('result' => false, 'message' => $validator->messages()), 400);
        }

        $model = MIPProduct::find($request->input('id'));
        if($request->input('name',null)             !== null) $model->name       = $request->input('name');
        if($request->input('sku',null)              !== null) $model->sku       = $request->input('sku');
        if($request->input('description',null)      !== null) $model->description       = $request->input('description');
        if($request->input('srp',null)              !== null) $model->srp       = $request->input('srp');

        $model->save();

        $return = array('result' => true, 'message' => ' New Stock has been added');
        return Response::json( $return  );

    }

    protected function destroy(Request $request)
    {

        //INITIALIZATION
        $input = $request->all();

         //validator example
        $validator = Validator::make($request->all(), [
            'id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(array('result' => false, 'message' => $validator->messages()), 400);
        }

        $model = MIPProduct::find($input['id']);
        $model->delete();


        $return = array('result' => true, 'message' => $i. ' Stock has been removed');
        return Response::json( $return  );

    }

}
