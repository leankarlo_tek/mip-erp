<?php

namespace App\Http\Controllers\API\v1\MIP;

use App\Models\User;
use App\Models\MIPCustomer;
use App\Models\MIPPayment;
use App\Models\MIPPaymentDetail;
use App\Models\MIPOrder;
use App\Models\MIPOrderDetail;
use App\Models\MIPPaymentType;
use App\Models\MIPProductStock;
use App\Models\MIPProduct;
use Redirect;
use View;
use Response;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class OrderListController extends Controller
{

    protected function show(Request $request) {
        /* show all invoice */
        $input = $request->all();
        if( isset($input['key']) && isset($input['to']) && isset($input['from']) && isset($input['id']) )
        {

            $from = $input['from'];
            $to = $input['to'];
            $key = $input['key'];
            $id = $input['id'];

            $end_date = date('Y-m-d',date(strtotime("+1 day", strtotime($to))));
            if($id != null || $id != '')
            {
                $sales = MIPOrder::where('id',$id)->with('user','customer','orderDetails.product')->get();
            }
            else if ($key == null || $key == '' )
            {
                $sales = MIPOrder::with('user','customer')
                ->whereIn('status' , [1,2])
                ->take(100)
                ->get();
            }
            else
            {
                
                $sales = MIPOrder::leftJoin('mip_order_details as details', 'details.order_id', '=', 'mip_orders.id')
                ->leftJoin('mip_products as product', 'product.id', '=', 'details.product_id')
                ->leftJoin('mip_customer as customer', 'customer.id', '=', 'mip_orders.customer_id')
                ->select('mip_orders.*')
                ->where('mip_orders.id', 'like', '%'.$key.'%')
                ->orWhere('product.name', 'like', '%'.$key.'%')
                ->orWhere('customer.email', 'like', '%'.$key.'%')
                ->orWhere('customer.lastname', 'like', '%'.$key.'%')
                ->orWhere('customer.firstname', 'like', '%'.$key.'%')
                ->with('user','customer')
                ->where('mip_orders.status', '!=', 3)
                ->groupBy('mip_orders.id')
                ->orderBy('created_at', 'desc')
                ->get();
            }

        }elseif ( isset($input['key']) ) {
         
            $key = $input['key'];
            
            $sales = MIPOrder::leftJoin('mip_order_details as details', 'details.order_id', '=', 'mip_orders.id')
                ->leftJoin('mip_products as product', 'product.id', '=', 'details.product_id')
                ->leftJoin('mip_customer as customer', 'customer.id', '=', 'mip_orders.customer_id')
                ->select('mip_orders.*')
                ->whereIn('status' , [1,2])
                ->where('mip_orders.id', 'like', '%'.$input['key'].'%')
                ->orWhere('product.name', 'like', '%'.$input['key'].'%')
                ->orWhere('customer.email', 'like', '%'.$input['key'].'%')
                ->orWhere('customer.lastname', 'like', '%'.$input['key'].'%')
                ->orWhere('customer.firstname', 'like', '%'.$input['key'].'%')
                ->with('user','customer')
                ->groupBy('mip_orders.id')
                ->orderBy('created_at', 'desc')
                ->get();

        }
        elseif ( isset($input['to']) && isset($input['from']) )
        {

            $from = $input['from'];
            $to = $input['to'];

            $end_date = date('Y-m-d',date(strtotime("+1 day", strtotime($to))));
            $sales = MIPOrder::with('user','customer')
            ->where('status', '!=' , 3)
            ->whereBetween('created_at', [$from, $end_date])
            ->get();


        }else
        {
            $sales = MIPOrder::with('user','customer')
            ->where('status', '!=' , 3)
            ->orderBy('created_at', 'desc')
            ->take(100)
            ->get();
        }

        if(count($sales) == 0){
            return Response::json(array('result' => true, 'data' => $sales, 'message' => 'no sales yet on selected dates.' ) );
        }else{
            return Response::json(array('result' => true, 'data' => $sales, 'message' => 'succesfully loaded the sales.' ) );
        }
    }

    protected function items(Request $request)
    {
        $input = $request->all();
        $model = MIPOrderDetail::where('order_id', $input['order_id'])->get();
        return Response::json(array('result' => true, 'data' => $model ) );
    }
}
