<?php

namespace App\Http\Controllers\API\v1\MIP;

use App\Models\MIPInventory;
use App\Models\MIPInventoryStock;
use DB;
use Redirect;
use View;
use Response;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class InventoryController extends Controller
{
    // USER DISPLAYS
    protected function show(Request $request)
    {
        $model = MIPInventory::all();
        $returnArray = array('result' => true);
        $returnArray = array_add($returnArray, 'data' , $model);
        return Response::json($returnArray );
    }

    protected function create(Request $request) {
        //INITIALIZATION
        $input = $request->all();
        
        $model = new MIPInventory;
        $model->name = $request->input('name');
        
        if($request->input('description',null)  !== null) $model->description   = $request->input('description');
        // dd($model);
        $model->save();

        $stockModel = new MIPInventoryStock;
        $stockModel->inventory_id = $model->id;
        $stockModel->isAvailable = 0;
        $stockModel->isActive = 0;

        $stockModel->save();

        return Response::json(array('result' => true ,'message' => 'Insert Success' ) );

    }

    protected function put(Request $request) {
        
        /* Find then generate the model */
        $model = MIPInventory::find($request->input('id'));

        /* Mandatory Field  */
		// No Mandatory Field for updating data

		/* Optional Field  */
        if($request->input('name',null)             !== null) $model->name              = $request->input('name');
        if($request->input('description',null)      !== null) $model->description       = $request->input('description');

        /* Place default values if field is missing  */
		// $model->updated = $request->input('updated') ? $request->input('updated') : new DateTime();Need to add update field in the future

		/* Saving the model created */
		$model->save();

		/* Return success code, message and data */
		return Response::json(array('code' => 200 ,'result' => true,'data' => $model ,'message' => 'updated success.' ));
    
    }

    protected function delete(Request $request)
	{
		/* Validate Important Fields */
		$validator = Validator::make($request->all(), [
			'id' 		=> 'required'
			]);

		/* If a field is missing or did not meet the right paramater return error. */
	    if ($validator->fails()) {
			return response()->json(array('result' => false, 'message' => $validator->messages()), 400);
		}

		/* Find then generate the model */
		$model = MIPInventory::where('id', $request->input('id') )->first();

		/* Deleting the model created */
		$model->delete();

		/* Return success code, message and data */
		return Response::json(array('code' => 200 ,'result' => true,'data' => $model ,'message' => 'deletion success' ));
	}
}
