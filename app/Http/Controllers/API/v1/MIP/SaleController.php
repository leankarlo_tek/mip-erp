<?php

namespace App\Http\Controllers\API\v1\MIP;

use App\Models\User;
use App\Models\MIPCustomer;
use App\Models\MIPPayment;
use App\Models\MIPPaymentDetail;
use App\Models\MIPOrder;
use App\Models\MIPOrderDetail;
use App\Models\MIPPaymentType;
use App\Models\MIPProductStock;
use App\Models\MIPProduct;
use Redirect;
use View;
use Response;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SaleController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Users Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the Images functions
    | 1. Upload
    | 2. Edit
    | 3. Delete
    | 4. Activate
    | 5. Deactivate
    | 6. Validation
    |
    */

    protected function SalesInformation_Range(Request $request) {

        $input = $request->all();
        if( isset($input['key']) && isset($input['to']) && isset($input['from']) )
        {

            $from = $input['from'];
            $to = $input['to'];
            $key = $input['key'];

            $end_date = date('Y-m-d',date(strtotime("+1 day", strtotime($to))));

            if ($key == null || $key == '')
            {
                $sales = MIPOrder::with('user','customer')
                ->whereBetween('created_at', [$from, $end_date])
                ->where('status', '>=' , 3)
                ->get();
            }
            else
            {
                $sales = MIPOrder::leftJoin('order_details as details', 'orders.id', '=', 'details.order_id')
                ->leftJoin('mip_products as product', 'details.product_id', '=', 'product.id')
                ->leftJoin('mip_customers as customer', 'orders.customer_id', '=', 'customer.id' )
                ->leftJoin('')
                ->select('orders.*')
                ->where('status', '>=' , 3)
                ->orWhere('orders.id', 'like', '%'.$input['key'].'%')
                ->orWhere('product.name', 'like', '%'.$input['key'].'%')
                ->orWhere('customer.email', 'like', '%'.$input['key'].'%')
                ->orWhere('customer.lastname', 'like', '%'.$input['key'].'%')
                ->with('user','customer')
                ->groupBy('orders.id')
                ->orderBy('created_at', 'desc')
                ->get();
            }

        }elseif ( isset($input['key']) ) {
         
            $key = $input['key'];

            $sales = MIPOrder::leftJoin('order_details as details', 'orders.id', '=', 'details.order_id')
            ->leftJoin('products as product', 'details.product_id', '=', 'product.id')
            ->leftJoin('customers as customer', 'customer.id', '=', 'orders.customer_id')
            ->select('orders.*')
            ->where('status', '>=' , 3)
            ->orWhere('orders.id', 'like', '%'.$input['key'].'%')
            ->orWhere('product.name', 'like', '%'.$input['key'].'%')
            ->orWhere('customer.email', 'like', '%'.$input['key'].'%')
            ->orWhere('customer.lastname', 'like', '%'.$input['key'].'%')
            ->with('user','customer')
            ->groupBy('orders.id')
            ->orderBy('created_at', 'desc')
            ->get();

        }
        elseif ( isset($input['to']) && isset($input['from']) )
        {

            $from = $input['from'];
            $to = $input['to'];

            $end_date = date('Y-m-d',date(strtotime("+1 day", strtotime($to))));
            $sales = MIPOrder::with('user','customer')
            ->where('status', '>=' , 3)
            ->whereBetween('created_at', [$from, $end_date])
            ->get();


        }else
        {
            $sales = MIPOrder::with('user','customer')
            ->where('status', '>=' , 3)
            ->orderBy('created_at', 'desc')
            ->take(100)
            ->get();
        }

        if(count($sales) == 0){
            return Response::json(array('result' => true, 'data' => $sales, 'message' => 'no sales yet on selected dates.' ) );
        }else{
            return Response::json(array('result' => true, 'data' => $sales, 'message' => 'succesfully loaded the sales.' ) );
        }
        

        // return Response::json(array('result' => true, 'message' => 'succesfully loaded the sales' ) );

    }

    protected function OrderDetails_Get(Request $request) {

        $input = $request->all();

        $orderID = $input['order_id'];

        $saleDetails = MIPOrderDetail::with('product','productStocks')->where('order_id', $orderID)->get();

        

        return Response::json(array('result' => true, 'data' => $saleDetails, 'message' => 'succesfully loaded the sales' ) );

        // return Response::json(array('result' => true, 'message' => 'succesfully loaded the sales' ) );

    }

    protected function PaymentDetails_Get(Request $request) {

        $input = $request->all();

        $orderID = $input['order_id'];

        $saleDetails = PaymentDetail::with('paymentMethod')->where('order_id', $orderID)->get();

        return Response::json(array('result' => true, 'data' => $saleDetails, 'message' => 'succesfully loaded the sales' ) );

        // return Response::json(array('result' => true, 'message' => 'succesfully loaded the sales' ) );

    }

    protected function PaymentDetails_Delete(Request $request) {

        $input = $request->all();

        $orderID = $input['order_id'];

        $affectedRows = PaymentDetail::where('order_id', $orderID)->delete();

        

        return Response::json(array('result' => true, 'data' => array('rows affected' => $affectedRows), 'message' => 'succesfully loaded the sales' ) );

        // return Response::json(array('result' => true, 'message' => 'succesfully loaded the sales' ) );

    }


}
