<?php

namespace App\Http\Controllers\API\v1\MIP;

use App\Models\User;
use Redirect;
use View;
use Response;
use Hash;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Users Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the Images functions
    | 1. Upload
    | 2. Edit
    | 3. Delete
    | 4. Activate
    | 5. Deactivate
    | 6. Validation
    |
    */

    // USER DISPLAYS
    protected function showAll()
    {

        $users = User::where('type', 0)->orWhere('type', 1)->orWhere('type', 2)->get();
        $returnArray = array('result' => true);
        $returnArray = array_add($returnArray, 'data' , $users);
        return Response::json($returnArray );
    }

    protected function login(Request $request)
    {

        //INITIALIZATION
        $input = $request->all();
        
        $email = $input['email'];
        $password = $input['password'];

        // PARAMETERS FOR AUTHENTICATION
        $userdata = array(
            'email'     => $input['email'],
            'password'  => $input['password'],
        );

        // AUTHENTICATE USER
        if (Auth::attempt($userdata) == true) 
        {
            if( Auth::user()->type == 0 || Auth::user()->type == 1 || Auth::user()->type == 2 )
            {
                $user = User::find(Auth::user()->id);
                return Response::json(array('result' => true, 'data' => $user, 'message' => 'succesfully signed in' ) );
            }else{
                Auth::logout();
                return Response::json(array('result' => false ,'message' => "Access denied. You don't have access to this particular system" ) );
            }
            
        }
        else{
            Auth::logout();
            return Response::json(array('result' => false ,'message' => 'incorrect email / password' ) );
        }

    }

    protected function createUser(Request $request) {
        //INITIALIZATION
        $input = $request->all();

        $email          = $input['email'];
        // $password       = $input['password'];//Hash::make('pw1234'),
        $type           = $input['type'];

        $user = new User;
        $user->email   = $email;        
        $user->password   = bcrypt('pw1234');

        $user->firstname    = $request->input('firstname') ? $request->input('firstname') : '';
        if($request->input('middlename',null)      !== null) $user->middlename       = $request->input('middlename');
        $user->lastname     = $request->input('lastname') ? $request->input('lastname') : '';

        $user->type   = $type;   
        $user->save();

        return Response::json(array('result' => true ,'message' => 'User Succesfully Saved!' ) );
    }

    protected function updatePassword(Request $request){
        $input              = $request->all();

        $id                 = $input['id'];
        $currentPassword    = $input['current_password'];
        $password           = $input['password'];

        $user = User::find($id);

        if(Hash::check($currentPassword, $user->password)){
            
            $user->password   = bcrypt($password);
            $user->save();

            return Response::json(array('result' => true ,'message' => 'Password Succesfully Saved!' ) );

        }else{
            return Response::json(array('result' => false ,'message' => 'Current passeword does not match Please Try again!!' ) );
        }

        
    }

    protected function updateUser(Request $request) {
        //INITIALIZATION
        $input = $request->all();
        try {
            $user = User::find($input['id']);

            if($request->input('email',null)      !== null) $user->email       = $request->input('email');
            if($request->input('type',null)      !== null) $user->type       = $request->input('type');
            if($request->input('firstname',null)      !== null) $user->firstname       = $request->input('firstname');
            if($request->input('middlename',null)      !== null) $user->middlename       = $request->input('middlename');
            if($request->input('lastname',null)      !== null) $user->lastname       = $request->input('lastname');

            $user->save();

            return Response::json(array('result' => true ,'message' => 'User Succesfully Saved!' ) );
        } catch (Exception $e) {
            return Response::json(array('result' => false ,'message' => 'Error Saving User! Contact System Admin' ) );
        }
        
    }

    protected function resetPasswordUser(Request $request)
    {
        $input = $request->all();
        $id = $input['id'];

        $user = User::find($id);
        try {
            $user->password   = bcrypt('pw1234');
            $user->save();
            return Response::json( array('result' => true ,'message' => 'User Password Set to Default!!' ) );

        } catch (Exception $e) {
            return Response::json( array('result' => false ,'message' => 'Error!! Contact System Admin' ) );
        }

    }

    protected function deleteUser(Request $request)
    {
        $input  = $request->all();
        $id     = $input['id'];
        $user   = User::find($id);
        try 
        {
            
            $user->delete();
            return Response::json( array('result' => true ,'message' => 'User Succesfully Deleted!' ) );

        } catch (Exception $e) {
            
        }

    }

    protected function logout(){
        // Auth::logout();
        return Response::json(array('data' => array('result' => true , 'message' => 'succesfully signed out' ) ));
    }

    protected function getUserData(Request $request)
    {
        $input = $request->all();
        $id = $input['id'];

        $user = User::find($id);
        if($user != null){
            return Response::json(array('result' => true ,'data' => $user ) );
        }else{
            return Response::json(array('result' => false  ) );
        }
        
    }

}
