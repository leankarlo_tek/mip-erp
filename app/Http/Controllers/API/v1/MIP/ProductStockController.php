<?php

namespace App\Http\Controllers\API\v1\mip;

use DB;
use App\Models\MIPProduct;
use App\Models\MIPProductStock;
use Redirect;
use View;
use Response;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;

class ProductStockController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Users Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the Images functions
    | 1. Upload
    | 2. Edit
    | 3. Delete
    | 4. Activate
    | 5. Deactivate
    | 6. Validation
    |
    */

    // USER DISPLAYS
    protected function show(Request $request)
    {

        $result = MIPProductStock::select( DB::raw('product_id, sum(case when isAvailable = 1 then 1 else 0 end) as NumberOfProducts '))
                     ->groupBy('product_id')
                     ->with('product')
                     ->get();

        $productInventory = array('result' => true);
        $productInventory = array_add($productInventory, 'data' , $result);
        return Response::json( $productInventory  );
    }

    protected function create(Request $request)
    {
        //validator example
        $validator = Validator::make($request->all(), [
            'product_id' => 'required',
            'quantity' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(array('result' => false, 'message' => $validator->messages()), 400);
        }

        //INITIALIZATION
        $input = $request->all();
        $quantity = $input['quantity'];
        $object = array();

        for ($i = 0; $i < $quantity; $i++)
        {
            array_push($object, array(
                'product_id' => $request->input('product_id'),
                'isAvailable' => 1
                ) ); 
        }

        if( $quantity != 0 ){
            MIPProductStock::insert($object);
            $return = array('result' => true, 'message' => $i. 'New Stock has been added');
            return Response::json( $return  );
        }else{
            $return = array('result' => false, 'message' => $i. 'Nothing to Add');
            return Response::json( $return  );
        }

        

    }

    protected function destroy(Request $request)
    {

        //INITIALIZATION
        $input = $request->all();

         //validator example
        $validator = Validator::make($request->all(), [
            'product_id' => 'required',
            'quantity' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(array('result' => false, 'message' => $validator->messages()), 400);
        }

        $quantity = $input['quantity'];
        $count = 0;

        for($i = 0;$i < $quantity; $i++){
            
            $productStock = MIPProductStock::where('product_id',$input['product_id'])
            ->where('isAvailable',1)
            ->orderBy('created_at', 'desc')
            ->first();

            $productStock->delete();
            $count++;
        }

        $return = array('result' => true, 'message' => $i. ' Stock has been removed');
        return Response::json( $return  );

    }

}
