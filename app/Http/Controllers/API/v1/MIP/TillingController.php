<?php

namespace App\Http\Controllers\API\v1\mip;

use DB;
use App\Models\MIPTilling;
use Redirect;
use View;
use Response;
use Auth;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TillingController extends Controller
{

    protected function show(Request $request)
    {
        $expected = MIPTilling::sum('expected_till');
        $deposited = MIPTilling::sum('deposited');

        $difference = $expected - $deposited;

        $model = array( 'id' => 1, 'expected_till' => (string)$difference,  'deposited' => (string)$deposited);
        
        return Response::json(array('result' => true, 'data' => array($model) ) );
    }

    protected function update(Request $request){
        
        $model = MIPTilling::find(1);
        
        $remaining = $model->expected_till - $request->input('deposited');

        /* Optional Field  */
        $model->expected_till              = $remaining;
		if($request->input('deposited',null)        !== null) $model->deposited       = $request->input('deposited');

        /* Place default values if field is missing  */
        // $model->expected_till = $remaining ? $remaining : 0;

        $model->save();
    }

    protected function createExpected(Request $request){
        
        $model = new MIPTilling;
        
        if($request->input('expected_till',null)    !== null) $model->expected_till = $request->input('expected_till');
        if($request->input('checkoutBy',null)    !== null) $model->expected_till = $request->input('checkoutBy');

        $model->save();

        return Response::json(array('code' => 200 ,'result' => true,'data' => $model ,'message' => 'succesfully added to cashier' ));

    }

    protected function createDeposit(Request $request){
        
        $model = new MIPTilling;
        
        if($request->input('deposited',null)    !== null) $model->deposited = $request->input('deposited');
        if($request->input('checkoutBy',null)    !== null) $model->expected_till = $request->input('checkoutBy');

        $model->save();

        return Response::json(array('code' => 200 ,'result' => true,'data' => $model ,'message' => 'succesfully deposited' ));

    }

}
