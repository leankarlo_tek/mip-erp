<?php

namespace App\Http\Controllers\API\v1\mip;

use DB;
use App\Models\MIPProduct;
use App\Models\MIPProductStock;
use App\Models\MIPProductType;
use App\Models\MIPOrder;
use App\Models\MIPOrderDetail;
use App\Models\MIPOrderStatus;
use App\Models\MIPPayment;
use App\Models\MIPPaymentDetail;
use App\Models\MIPPaymentStatus;
use App\Models\MIPPaymentType;
use Redirect;
use View;
use Response;
use Auth;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    protected function test(Request $request)
    {
        $input = $request->all();
        return Response::json($input);
    
    }

    protected function PaymentTypeShow(Request $request)
    {

        $model = MIPPaymentType::all();

        $result = array('result' => true);
        $result = array_add($result, 'data' , $model);
        return Response::json( $result  );
        
    }

    protected function PaymentStatusShow(Request $request)
    {

        $model = MIPPaymentStatus::all();

        $result = array('result' => true);
        $result = array_add($result, 'data' , $model);
        return Response::json( $result  );
        
    }

    protected function PaymentDetails_Show(Request $request){
        $input = $request->all();

        $orderID = $input['order_id'];

        $saleDetails = MIPPaymentDetail::with('paymentMethod')->where('order_id', $orderID)->get();

        return Response::json(array('result' => true, 'data' => $saleDetails, 'message' => 'succesfully loaded the sales' ) );

        // return Response::json(array('result' => true, 'message' => 'succesfully loaded the sales' ) );
    }


}
