<?php

namespace App\Http\Controllers\API\v1\MIP;

use App\Models\MIPSettings;
use DB;
use Redirect;
use View;
use Response;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SettingsController extends Controller
{
    // USER DISPLAYS
    protected function show(Request $request)
    {
        $model = MIPSettings::all();
        $returnArray = array('result' => true);
        $returnArray = array_add($returnArray, 'data' , $model);
        return Response::json($returnArray );
    }


    protected function put(Request $request) {
        
        /* Find then generate the model */
        $model = MIPSettings::find(1);

        /* Mandatory Field  */
		// No Mandatory Field for updating data

		/* Optional Field  */
        if($request->input('store_name',null)   !== null) $model->store_name    = $request->input('store_name');
        if($request->input('operator',null)     !== null) $model->operator      = $request->input('operator');
        if($request->input('harvester',null)     !== null) $model->harvester      = $request->input('harvester');
        if($request->input('packager',null)     !== null) $model->packager      = $request->input('packager');

        /* Place default values if field is missing  */
		// $model->updated = $request->input('updated') ? $request->input('updated') : new DateTime();Need to add update field in the future

		/* Saving the model created */
		$model->save();

		/* Return success code, message and data */
		return Response::json(array('code' => 200 ,'result' => true,'data' => $model ,'message' => 'updated success.' ));
    
    }

}
