<?php

namespace App\Http\Controllers\API\v1\MIP;

use App\Models\MIPCourier;
use Redirect;
use View;
use Response;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CourierController extends Controller
{


    // USER DISPLAYS
    protected function show()
    {
        $users = MIPCourier::all();
        $returnArray = array('result' => true);
        $returnArray = array_add($returnArray, 'data' , $users);
        return Response::json($returnArray );
    }

    protected function create(Request $request) {
        //INITIALIZATION
        // $input = $request->all();

        $model = new MIPCourier;
        $model->name = $request->input('name');
        if($request->input('description',null)      !== null) $model->description       = $request->input('description');
        
        if($request->input('contact_number_1',null)      !== null) $model->contact_number_1       = $request->input('contact_number_1');
        if($request->input('contact_number_2',null)      !== null) $model->contact_number_2       = $request->input('contact_number_2');
        if($request->input('contact_number_3',null)      !== null) $model->contact_number_3       = $request->input('contact_number_3');

        if($request->input('fax_1',null)      !== null) $model->fax_1       = $request->input('fax_1');
        if($request->input('fax_2',null)      !== null) $model->fax_2       = $request->input('fax_2');
        if($request->input('fax_3',null)      !== null) $model->fax_3       = $request->input('fax_3');

        if($request->input('email',null)      !== null) $model->email       = $request->input('email');

        if($request->input('address_1',null)      !== null) $model->address_1       = $request->input('address_1');
        if($request->input('address_2',null)      !== null) $model->address_2       = $request->input('address_2');
        if($request->input('address_3',null)      !== null) $model->address_3       = $request->input('address_3');

        if($request->input('state',null)      !== null) $model->state       = $request->input('state');
        if($request->input('zipcode',null)      !== null) $model->zipcode       = $request->input('zipcode');
        if($request->input('country',null)      !== null) $model->country       = $request->input('country');

        // if($request->input('isActive',null)      !== null) $model->isActive       = $request->input('isActive');
        
        $model->save();

        return Response::json(array('result' => true ,'message' => 'Insert Success' ) );

    }


    protected function put(Request $request) {
        
        

        /* Find then generate the model */
        $model = MIPCourier::find($request->input('id'));

        /* Mandatory Field  */
		// No Mandatory Field for updating data

		/* Optional Field  */
        if($request->input('name',null)             !== null) $model->name              = $request->input('name');
		if($request->input('description',null)      !== null) $model->description       = $request->input('description');
        
        if($request->input('contact_number_1',null) !== null) $model->contact_number_1  = $request->input('contact_number_1');
        if($request->input('contact_number_2',null) !== null) $model->contact_number_2  = $request->input('contact_number_2');
        if($request->input('contact_number_3',null) !== null) $model->contact_number_3  = $request->input('contact_number_3');

        if($request->input('fax_1',null)            !== null) $model->fax_1             = $request->input('fax_1');
        if($request->input('fax_2',null)            !== null) $model->fax_2             = $request->input('fax_2');
        if($request->input('fax_3',null)            !== null) $model->fax_3             = $request->input('fax_3');

        if($request->input('email',null)            !== null) $model->email             = $request->input('email');

        if($request->input('address_1',null)        !== null) $model->address_1         = $request->input('address_1');
        if($request->input('address_2',null)        !== null) $model->address_2         = $request->input('address_2');
        if($request->input('address_3',null)        !== null) $model->address_3         = $request->input('address_3');

        if($request->input('state',null)            !== null) $model->state             = $request->input('state');
        if($request->input('zipcode',null)          !== null) $model->zipcode           = $request->input('zipcode');
        if($request->input('country',null)          !== null) $model->country           = $request->input('country');

        if($request->input('isActive',null)         !== null) $model->isActive          = $request->input('isActive');
        
        /* Place default values if field is missing  */
		// $model->updated = $request->input('updated') ? $request->input('updated') : new DateTime();Need to add update field in the future

		/* Saving the model created */
		$model->save();

		/* Return success code, message and data */
		return Response::json(array('code' => 200 ,'result' => true,'data' => $model ,'message' => 'updated success.' ));
    
    }

    protected function delete(Request $request)
	{
		/* Validate Important Fields */
		$validator = Validator::make($request->all(), [
			'id' 		=> 'required'
			]);

		/* If a field is missing or did not meet the right paramater return error. */
		if ($validator->fails()) {
			return response()->json(array('result' => false, 'message' => $validator->messages()), 400);
		}

		/* Find then generate the model */
		$model = MIPCourier::where('id', $request->input('id') )->first();

		/* Deleting the model created */
		$model->delete();

		/* Return success code, message and data */
		return Response::json(array('code' => 200 ,'result' => true,'data' => $model ,'message' => 'deletion success' ));
	}
}
