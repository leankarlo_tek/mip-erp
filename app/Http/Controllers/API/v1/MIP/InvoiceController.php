<?php

namespace App\Http\Controllers\API\v1\MIP;

use App\Models\User;
use App\Models\MIPCustomer;
use App\Models\MIPPayment;
use App\Models\MIPPaymentDetail;
use App\Models\MIPOrder;
use App\Models\MIPOrderDetail;
use App\Models\MIPPaymentType;
use App\Models\MIPProductStock;
use App\Models\MIPProduct;
use Redirect;
use View;
use Response;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class InvoiceController extends Controller
{

    protected function show(Request $request) {
        /* show all invoice */
        $input = $request->all();
        if( isset($input['key']) && isset($input['to']) && isset($input['from']) )
        {

            $from = $input['from'];
            $to = $input['to'];
            $key = $input['key'];

            $end_date = date('Y-m-d',date(strtotime("+1 day", strtotime($to))));

            if ($key == null || $key == '')
            {
                $sales = MIPOrder::with('user','customer')
                ->where('status', '=' , 3)
                ->orderBy('created_at', 'desc')
                ->take(100)
                ->get();
            }
            else
            {
                // dd("test");
                $sales = MIPOrder::leftJoin('mip_order_details as details', 'details.order_id', '=', 'mip_orders.id')
                ->leftJoin('mip_products as product', 'product.id', '=', 'details.product_id')
                ->leftJoin('mip_customer as customer', 'customer.id', '=', 'mip_orders.customer_id')
                ->select('mip_orders.*')
                ->where('mip_orders.status', 3)
                ->where('mip_orders.id', 'like', '%'.$input['key'].'%')
                ->orWhere('product.name', 'like', '%'.$input['key'].'%')
                ->orWhere('customer.email', 'like', '%'.$input['key'].'%')
                ->orWhere('customer.lastname', 'like', '%'.$input['key'].'%')
                ->orWhere('customer.firstname', 'like', '%'.$input['key'].'%')
                ->with('user','customer')
                ->groupBy('mip_orders.id')
                ->orderBy('created_at', 'desc')
                ->get();
            }

        }elseif ( isset($input['key']) && $input['key'] != "" ) {
            $key = $input['key'];
            
            $sales = MIPOrder::leftJoin('mip_order_details as details', 'details.order_id', '=', 'mip_orders.id')
                ->leftJoin('mip_products as product', 'product.id', '=', 'details.product_id')
                ->leftJoin('mip_customer as customer', 'customer.id', '=', 'mip_orders.customer_id')
                ->select('mip_orders.*')
                ->where('mip_orders.status', '=' , 3)
                ->where('mip_orders.id', 'like', '%'.$input['key'].'%')
                ->orWhere('product.name', 'like', '%'.$input['key'].'%')
                ->orWhere('customer.email', 'like', '%'.$input['key'].'%')
                ->orWhere('customer.lastname', 'like', '%'.$input['key'].'%')
                ->with('user','customer')
                ->groupBy('mip_orders.id')
                ->orderBy('created_at', 'desc')
                ->get();

        }
        // elseif ( isset($input['to']) && isset($input['from']) )
        // {

        //     $from = $input['from'];
        //     $to = $input['to'];

        //     $end_date = date('Y-m-d',date(strtotime("+1 day", strtotime($to))));
        //     $sales = MIPOrder::with('user','customer')
        //     ->where('status', '=' , 3)
        //     ->whereBetween('created_at', [$from, $end_date])
        //     ->get();


        // }
        else
        {
            $sales = MIPOrder::with('user','customer')
            ->where('status', '=' , 3)
            ->orderBy('created_at', 'desc')
            ->take(100)
            ->get();
        }

        if(count($sales) == 0){
            return Response::json(array('result' => true, 'data' => $sales, 'message' => 'no sales yet on selected dates.' ) );
        }else{
            return Response::json(array('result' => true, 'data' => $sales, 'message' => 'succesfully loaded the sales.' ) );
        }


    }
}
