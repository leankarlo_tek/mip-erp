<?php

namespace App\Http\Controllers\API\v1\MIP;

use App\Models\MIPInventory;
use App\Models\MIPInventoryStock;
use DB;
use Redirect;
use View;
use Response;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class InventoryStockController extends Controller
{
    // USER DISPLAYS
    protected function show(Request $request)
    {
        $model = MIPInventoryStock::select( DB::raw('*, sum(case when isAvailable = 1 then 1 else 0 end) as NumberOfProducts '))
                     ->groupBy('inventory_id')
                     ->with('information')
                     ->get();

        $returnArray = array('result' => true);
        $returnArray = array_add($returnArray, 'data' , $model);
        return Response::json($returnArray );
    }

    protected function create(Request $request) {
        //INITIALIZATION
        $quantity = $request->input('quantity');
        $object = array();
        
        for ($i = 0; $i < $quantity; $i++)
        {
            array_push($object, array(
                'inventory_id' => $request->input('inventory_id'),
                'used_by' => 0
                 ) ); 
        }

        MIPInventoryStock::insert($object);
        return Response::json(array('result' => true ,'message' => 'Insert Success' ) );
    }

    protected function useStock(Request $request){
        
        $quantity = $request->input('quantity');

        $model = MIPInventoryStock::where('inventory_id', $request->input('inventory_id'))
        ->where('isAvailable', 1)
        ->first();
    // dd($model);
        $model->used_by = $request->input('used_by');
        $model->isAvailable = 0;
        
        $model->save();

        return Response::json(array('result' => true ,'message' => 'Update Success' ) );
    }

    protected function put(Request $request) 
    {
        /* Find then generate the model */
        $model = MIPInventoryStock::find($request->input('id'));

        /* Mandatory Field  */
		// No Mandatory Field for updating data

		/* Optional Field  */
        if($request->input('isAvailable',null)  !== null) $model->isAvailable       = $request->input('isAvailable');
        if($request->input('inventory_id',null) !== null) $model->inventory_id      = $request->input('inventory_id');
        if($request->input('isActive',null)     !== null) $model->isActive          = $request->input('isActive');
        if($request->input('used_by',null)      !== null) $model->used_by           = $request->input('used_by');
        if($request->input('date_used',null)    !== null) $model->date_used         = $request->input('date_used');
        
        /* Place default values if field is missing  */
		// $model->updated = $request->input('updated') ? $request->input('updated') : new DateTime();Need to add update field in the future

		/* Saving the model created */
		$model->save();

		/* Return success code, message and data */
		return Response::json(array('code' => 200 ,'result' => true,'data' => $model ,'message' => 'updated success.' ));
    
    }

    protected function delete(Request $request)
	{
		/* Validate Important Fields */
		$validator = Validator::make($request->all(), [
			'id' 		=> 'required'
			]);

		/* If a field is missing or did not meet the right paramater return error. */
		if ($validator->fails()) {
			return response()->json(array('result' => false, 'message' => $validator->messages()), 400);
		}

		/* Find then generate the model */
		$model = MIPInventoryStock::where('id', $request->input('id') )->first();

		/* Deleting the model created */
		$model->delete();

		/* Return success code, message and data */
		return Response::json(array('code' => 200 ,'result' => true,'data' => $model ,'message' => 'deletion success' ));
	}
}
