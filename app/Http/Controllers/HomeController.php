<?php namespace App\Http\Controllers;

use DB;
use App\Models\MIPProduct;
use App\Models\MIPProductStock;
use App\Models\MIPProductType;
use App\Models\MIPOrder;
use App\Models\MIPOrderDetail;
use App\Models\MIPOrderStatus;
use App\Models\MIPPayment;
use App\Models\MIPPaymentDetail;
use App\Models\MIPPaymentStatus;
use Redirect;
use View;
use Response;
use Auth;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeController extends Controller {

    public function index()
    {
        return view('welcome');
    }

    public function checkConnection()
    {
        return Response::json(array('data' => array('result' => true,'message' => getenv('APP_ENV') ) ) );
    }

}