<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::post('oauth/access_token', function() {

    return Response::json(Authorizer::issueAccessToken());
	
});

// Initialize Angular.js App Route.
// Route::get('/', 'HomeController@index');

// Route::get('/pomail', function () {
// 		    return view('email/purchaseorder');
// 		});

Route::get('/test', 'API\v1\MIP\InvoiceController@show');

Route::post('/test', 'API\v1\MIP\CheckoutController@test');

Route::put('/test', 'API\v1\MIP\CheckoutController@test');

Route::get('/testconnection', 'HomeController@checkConnection');


/* API'S VERSION 1 */

	// universal 
	Route::group(array('prefix'=>'mip'), function(){

		Route::group(array('prefix'=>'productstock'), function(){

			Route::get('/', 'API\v1\MIP\ProductStockController@show'); 

			Route::post('/', 'API\v1\MIP\ProductStockController@create'); 

		});	

		Route::group(array('prefix'=>'inventory'), function(){

			Route::get('/', 'API\v1\MIP\InventoryController@show'); //tested pass

			Route::post('/', 'API\v1\MIP\InventoryController@create'); //tested pass

			Route::put('/', 'API\v1\MIP\InventoryController@put'); //tested pass

			Route::delete('/', 'API\v1\MIP\InventoryController@delete'); //tested pass

		});	

		Route::group(array('prefix'=>'settings'), function(){

			Route::get('/', 'API\v1\MIP\SettingsController@show'); //tested pass

			Route::put('/', 'API\v1\MIP\SettingsController@put'); //tested pass

		});	

		Route::group(array('prefix'=>'inventory_stock'), function(){

			Route::get('/', 'API\v1\MIP\InventoryStockController@show'); //tested pass

			Route::put('/use', 'API\v1\MIP\InventoryStockController@useStock'); 
			
			Route::post('/', 'API\v1\MIP\InventoryStockController@create'); //tested pass

			Route::put('/', 'API\v1\MIP\InventoryStockController@put'); //tested pass

			Route::delete('/', 'API\v1\MIP\InventoryStockController@delete'); //tested pass

		});	

		Route::group(array('prefix'=>'product'), function(){

			Route::get('/', 'API\v1\MIP\ProductController@show'); //tested pass

			Route::post('/', 'API\v1\MIP\ProductController@create'); //tested pass

			Route::put('/', 'API\v1\MIP\ProductController@update'); //tested pass

			Route::delete('/', 'API\v1\MIP\ProductController@destroy'); //tested pass

		});	

		Route::group(array('prefix'=>'checkout'), function(){

			Route::post('/', 'API\v1\MIP\CheckoutController@createOrderAndPayment'); //tested pass

			Route::post('/details', 'API\v1\MIP\CheckoutController@createOrderDetails'); //tested pass

			Route::post('/payment/details', 'API\v1\MIP\CheckoutController@createPaymentDetails'); //tested pass

			Route::put('/payment', 'API\v1\MIP\CheckoutController@updatePaymentSatus'); //tested pass

		});	

		Route::group(array('prefix'=>'payment'), function(){

			Route::get('/type', 'API\v1\MIP\PaymentController@PaymentTypeShow'); //tested pass

			Route::get('/status', 'API\v1\MIP\PaymentController@PaymentStatusShow'); //tested pass

			Route::get('/details', 'API\v1\MIP\PaymentController@PaymentDetails_Show');

		});	

		Route::group(array('prefix'=>'users'), function(){

			Route::get('/get', 				'API\v1\MIP\UserController@getUserData');  //tested pass

			Route::get('/reset_password', 	'API\v1\MIP\UserController@resetPasswordUser');  //tested pass

			Route::get('/showall', 			'API\v1\MIP\UserController@showAll');  //tested pass

			Route::post('/login', 			'API\v1\MIP\UserController@login'); //tested pass

			Route::get('/logout', 			'API\v1\MIP\UserController@logout'); //tested pass

			Route::post('/create', 			'API\v1\MIP\UserController@createUser'); //tested pass

			Route::post('/update', 			'API\v1\MIP\UserController@updateUser'); //tested pass

			Route::put('/password/update', 	'API\v1\MIP\UserController@updatePassword'); //tested pass

			Route::delete('/delete', 			'API\v1\MIP\UserController@deleteUser'); //tested pass

			Route::post('/change_password', 'API\v1\MIP\UserController@updateUserPassword'); //tested pass
		
		});

		Route::group(array('prefix'=>'report'), function(){

			//Harvest Reports
			Route::group(array('prefix'=>'harvest/current/month'), function(){
				Route::get('/', 'API\v1\MIP\HarvestReportController@show');
			});

		});

		Route::group(array('prefix'=>'couriers'), function(){

			Route::get('/', 'API\v1\MIP\CourierController@show'); //tested pass

			Route::post('/', 'API\v1\MIP\CourierController@create'); //tested pass

			Route::put('/', 'API\v1\MIP\CourierController@put');

			Route::delete('/', 'API\v1\MIP\CourierController@delete');

		});	

		Route::group(array('prefix'=>'customers'), function(){
 
			Route::get('/', 		'API\v1\MIP\CustomerController@showAll'); //tested

			Route::post('/', 		'API\v1\MIP\CustomerController@createUser'); //tested

			Route::put('/', 		'API\v1\MIP\CustomerController@updateUser'); //tested

			Route::delete('/', 		'API\v1\MIP\CustomerController@deleteUser'); //tested

		});

		Route::group(array('prefix'=>'invoice'), function(){

			Route::get('/', 			'API\v1\MIP\InvoiceController@show');
			
			Route::get('/details', 	'API\v1\MIP\SaleController@OrderDetails_Get');

		});

		Route::group(array('prefix'=>'tilling'), function(){

			Route::get('/', 			'API\v1\MIP\TillingController@show');

			Route::post('/deposit', 			'API\v1\MIP\TillingController@createDeposit');

		});

		Route::group(array('prefix'=>'order_quote'), function(){

			Route::get('/', 			'API\v1\MIP\OrderListController@show');

		});

	});	
	

/* END API'S VERSION 1 */