<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourierTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mip_couriers', function (Blueprint $table) {
            
            $table->increments('id');

            $table->string('name');
            $table->longText('description')->nullable();

            $table->longText('contact_number_1')->nullable();
            $table->longText('contact_number_2')->nullable();
            $table->longText('contact_number_3')->nullable();

            $table->longText('fax_1')->nullable();
            $table->longText('fax_2')->nullable();
            $table->longText('fax_3')->nullable();

            $table->longText('email')->nullable();

            $table->longText('address_1')->nullable();
            $table->longText('address_2')->nullable();
            $table->longText('address_3')->nullable();
            $table->longText('state')->nullable();
            $table->longText('zipcode')->nullable();
            $table->longText('country')->nullable();

            $table->boolean('isActive')->default(1);

            $table->dateTime('created_at');
            $table->timestamp('updated_at');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mip_couriers');
    }
}
