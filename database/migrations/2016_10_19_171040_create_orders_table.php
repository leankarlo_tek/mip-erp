<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mip_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id');
            $table->double('vat')->default(0);
            $table->double('subtotal')->default(0);
            $table->double('discount')->default(0);
            $table->double('total')->default(0);
            $table->integer('status')->default(1);
            $table->integer('created_by');

            $table->dateTime('created_at');
            $table->timestamp('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mip_orders');
    }
}
