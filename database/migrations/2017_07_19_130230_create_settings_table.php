<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mip_settings', function (Blueprint $table) {
            
            $table->increments('id');

            $table->string('store_name')->nullable();
            $table->float('operator')->nullable();
            $table->float('harvester')->nullable();

            $table->float('packager')->nullable();

            $table->double('created_at');
            $table->timestamp('updated_at');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mip_settings');
    }
}
