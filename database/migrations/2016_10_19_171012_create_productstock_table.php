<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductstockTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mip_product_stocks', function (Blueprint $table) {
            
            $table->increments('id');
            $table->integer('product_id');
            $table->tinyInteger('isAvailable')->default(1);

            $table->dateTime('created_at');
            $table->timestamp('updated_at');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mip_product_stocks');
    }
}
