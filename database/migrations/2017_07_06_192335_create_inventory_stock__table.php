<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoryStockTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mip_inventory_stock', function (Blueprint $table) {
            
            $table->increments('id');

            $table->integer('isAvailable')->default(1);
            $table->integer('inventory_id');
            $table->boolean('isActive')->default(1);

            $table->integer('used_by')->default(1);
            $table->dateTime('date_used')->nullable();

            $table->dateTime('created_at');
            $table->timestamp('updated_at');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mip_inventory_stock');
    }
}
