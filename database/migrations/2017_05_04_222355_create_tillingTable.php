<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTillingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('mip_tilling', function (Blueprint $table) {
            
            $table->increments('id');
            $table->double('expected_till')->default(0);
            $table->double('deposited')->default(0);
            $table->integer('created_by')->nullable();

            $table->dateTime('created_at');
            $table->timestamp('updated_at');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('mip_tilling');
    }

}
