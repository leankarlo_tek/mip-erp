<?php

use Illuminate\Database\Seeder;

class PaymentTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('mip_payment_type')->insert([
            ['name' => 'Cash DR', 'description' => 'Non Vatable Sales' ],
            ['name' => 'Cash Invoice', 'description' => 'Vatable Cash Payment Sale' ],
            ['name' => 'Credit Card', 'description' => 'Credit Card Payment' ],
            ['name' => 'Cheque', 'description' => 'Cheque Payment' ],
        ]);
    }
}
