<?php

use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('mip_products')->insert([
            ['name' => 'Tube Ice 1 Kg',                     'sku' => 'TBH-001',  'srp' => 35 ],
            ['name' => 'Tube Ice 5 Kg',                     'sku' => 'TBH-005',  'srp' => 35 ],
            ['name' => 'Tube Ice 10 Kg',                    'sku' => 'TBH-010',  'srp' => 35 ],
            ['name' => 'Tube Ice 15 Kg',                    'sku' => 'TBH-015',  'srp' => 35 ],
            ['name' => 'Block of Ice',                      'sku' => 'BLK-001',  'srp' => 35 ],
            ['name' => 'Tube Ice For poultry - Sack 35Kg',  'sku' => 'TBP-001',  'srp' => 35 ],
        ]);
    }
}
