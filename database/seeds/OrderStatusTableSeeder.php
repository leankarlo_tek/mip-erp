<?php

use Illuminate\Database\Seeder;

class OrderStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('mip_order_status')->insert([
            [
                'name' => 'Qoute',
                'descriptions' => 'Qouted an order for a Client'
            ],
            [
                'name' => 'Order',
                'descriptions' => 'Client orders an item but is still not paid or delivered'
            ],
            [
                'name' => 'Invoice',
                'descriptions' => 'Items Paid'
            ],
            [
                'name' => 'Paid/Delivered',
                'descriptions' => 'Paid and Delivered'
            ]
        ]);
    }
}
