<?php

use Illuminate\Database\Seeder;
use App\Models\MIPProduct;
use App\Models\MIPProductStock;

class ProductStockSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->command->info('Start filling inventory');
        $y = 0;
        $x = 0;

        $seedEach = 30;

        $products = MIPProduct::all();
        $x = $products->count() * $seedEach;
        $this->command->info($x);

        foreach($products as $product)
        {	
            for($i=0;$i<$seedEach;$i++)
            {
                $model = new MIPProductStock;
                $model->product_id = $product->id;
                $model->save();
    
                $y++;
                $p = $y/$x;
                $result = $p * 100;
    
                if ($result % 1 == 0)
                {
                    $this->command->info('Loading .................................. '. $result . '%');
                }
            }

        }


        $this->command->info('Ended filling inventory');
    }
}
