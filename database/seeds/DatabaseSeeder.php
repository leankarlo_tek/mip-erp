<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		//Model::unguard();
		$this->call(UsersTableSeeder::class);
		$this->call(ProductTableSeeder::class);
 		$this->call(OrderStatusTableSeeder::class);
 		$this->call(PaymentTypeSeeder::class);
 		$this->call(PaymentStatusSeeder::class);
 		$this->call(UsersTypeSeeder::class);
		$this->call(CustomerTableSeeder::class);
	}

}
