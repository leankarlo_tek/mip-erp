<?php

use Illuminate\Database\Seeder;

class PaymentStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('mip_payment_status')->insert([
            ['name' => 'pending', 'description' => 'not yet paid' ],
            ['name' => 'downpayment', 'description' => 'paid half or any % of the total' ],
            ['name' => 'fullpayment', 'description' => 'paid already' ],
        ]);
    }
}
