<?php

use Illuminate\Database\Seeder;

class CustomerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('mip_customer')->insert([
            [ 'email' => 'default@mip.ph',
            'firstname' => 'Anonymous',
            'lastname' =>'Customer',
            'created_by' =>'0' ],
        ]);
    }
}
