<?php

use Illuminate\Database\Seeder;

class UsersTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        DB::table('user_types')->insert([
            
            [ 'name' => 'Administator',
            'description' => 'Application Admin'
            ],

            [ 'name' => 'MIP Manager',
            'description' => 'Can only access MIP applications'
            ],

            [ 'name' => 'MIP Cashier',
            'description' => 'Can only access MIP cashier'
            ],

            [ 'name' => 'MIP Packager',
            'description' => 'Can only inventory app'
            ],

            [ 'name' => 'MIP Harvester',
            'description' => 'Can only inventory app'
            ],

            [ 'name' => 'MIP Operator',
            'description' => 'Can only inventory app'
            ],

        ]);
    }
}
