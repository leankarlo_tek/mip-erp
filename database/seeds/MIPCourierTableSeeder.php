<?php

use Illuminate\Database\Seeder;

class MIPCourierTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('mip_couriers')->insert([
            ['name' => 'MIP Delivery' ],
            ['name' => 'LBC' ],
        ]);
    }
}
