<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        DB::table('users')->insert([
            [ 'email' => 'accounts@teknolohiya.ph',
            'password' => bcrypt('pw1234'),
            'firstname' => 'Teknolohiya',
            'middlename' =>'Artisans',
            'lastname' =>'Admin',
            'created_by' =>'0' ],

            [ 'email' => 'nathan.uy@neurealtydevt.com',
            'password' => bcrypt('pw1234'),
            'firstname' => 'Nathan',
            'middlename' =>'',
            'lastname' =>'Uy',
            'created_by' =>'0' ],

        ]);
    }
}
